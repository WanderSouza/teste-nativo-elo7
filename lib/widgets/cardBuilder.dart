import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:testeelo7/models/product.dart';
import 'package:url_launcher/url_launcher.dart';

class CardBuilder extends StatelessWidget {
  Product product;
  CardBuilder(this.product);

  Future<void> _launchLink(String url) async {
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: true, forceWebView: true);
    } else
      throw 'Could not open $url';
  }

  String convertToUTF8(String text) => utf8.decode(text.codeUnits);

  @override
  Widget build(BuildContext context) {
    return Card(
        child: InkWell(
            onTap: () {
              _launchLink(this.product.link);
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Expanded(
                  flex: 3,
                  child: Image(
                    image: NetworkImage(this.product.picture),
                    fit: BoxFit.cover,
                  ),
                ),
                Text(convertToUTF8(this.product.title),
                    style: TextStyle(color: Colors.greenAccent)),
                Row(children: <Widget>[
                  Expanded(
                    flex: 5,
                    child: Text(
                      convertToUTF8(this.product.price.current),
                      style: TextStyle(color: Colors.deepOrangeAccent),
                    ),
                  ),
                  Expanded(
                    flex: 9,
                    child: Text(
                        convertToUTF8(this.product.price.nonPromotional),
                        style: TextStyle(
                            color: Colors.grey,
                            decoration: TextDecoration.lineThrough)),
                  ),
                ]),
                Text(convertToUTF8(this.product.price.installment),
                    style: TextStyle(color: Colors.blueGrey))
              ],
            )));
  }
}
