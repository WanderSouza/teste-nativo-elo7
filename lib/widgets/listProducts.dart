import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:testeelo7/models/product.dart';
import 'package:testeelo7/widgets/cardBuilder.dart';

class ListProducts extends StatefulWidget {
  @override
  _ListProductsState createState() => _ListProductsState();
}

class _ListProductsState extends State<ListProducts> {
  ScrollController controller;
  List<CardBuilder> items = new List<CardBuilder>();
  int page = 0;

  void setUpList() async {
    final http.Response response = await http.get(
        "https://5dc05c0f95f4b90014ddc651.mockapi.io/elo7/api/1/products?page=$page");
    final resposta = jsonDecode(response.body);
    items.addAll(new List.generate(List.from(resposta).length,
        (index) => CardBuilder(Product.fromJson(resposta[index]))));
  }

  @override
  void initState() {
    super.initState();
    setUpList();
    controller = new ScrollController()..addListener(_scrollListener);
  }

  @override
  void dispose() {
    controller.removeListener(_scrollListener);
    super.dispose();
  }

  static String requestUrl =
      "https://5dc05c0f95f4b90014ddc651.mockapi.io/elo7/api/1/products";
  var _controller = TextEditingController();
  void changeUrl(String newUrl) {
    setState(() {
      requestUrl = newUrl;
    });
  }

  void _scrollListener() async {
    final http.Response response = await http.get(
        "https://5dc05c0f95f4b90014ddc651.mockapi.io/elo7/api/1/products?page=$page");
    final resposta = jsonDecode(response.body);

    if (controller.position.extentAfter < 500) {
      setState(() {
        items.addAll(new List.generate(List.from(resposta).length,
            (index) => CardBuilder(Product.fromJson(resposta[index]))));
      });
    }
    page++;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.redAccent,
          actions: [
            IconButton(
                onPressed: () => {_controller.clear()},
                icon: Icon(Icons.clear)),
            IconButton(icon: Icon(Icons.mic)),
          ],
          leading: IconButton(icon: Icon(Icons.arrow_back)),
          title: TextField(
            controller: _controller,
            decoration: InputDecoration(hintText: "Buscar"),
            onSubmitted: (value) {
              changeUrl(
                  "https://5dc05c0f95f4b90014ddc651.mockapi.io/elo7/api/1/products?q=$value");
            },
          ),
        ),
        body: GridView.builder(
          controller: controller,
          gridDelegate:
              SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
          itemBuilder: (context, index) {
            return items[index];
          },
          itemCount: items.length,
        ));
  }
}
