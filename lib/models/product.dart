import 'package:testeelo7/models/price.dart';

class Product {
  Price price;
  String picture;
  String title;
  String id;
  String link;

Product({this.price, this.picture, this.title, this.id, this.link});

factory Product.fromJson(Map<String, dynamic> json) {
    return Product(
      price: Price.fromJson(json["price"]) ?? "",
      picture: json["picture"] ?? "",
      title: json["title"] ?? "",
      id: json["id"] ?? "",
      link: json["_link"] ?? "",
    );
}
}