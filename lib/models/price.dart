class Price {
  String current, installment, nonPromotional;

  Price({this.current, this.installment = "", this.nonPromotional = ""});

  factory Price.fromJson(Map<String, dynamic> json) {
    return Price(
        current: json["current"] ?? "",
        installment: json["installment"] ?? "",
        nonPromotional: json["nonPromotional"] ?? "");
  }
}
